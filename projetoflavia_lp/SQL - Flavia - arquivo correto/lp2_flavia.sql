-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 08-Out-2019 às 05:58
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lp2_flavia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `course_img` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `course_duration` decimal(3,1) NOT NULL,
  `course_description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `courses`
--

INSERT INTO `courses` (`course_id`, `course_name`, `course_img`, `course_duration`, `course_description`) VALUES
(7, 'Alemão', '/public-template/tema-site/img/courses/fundoverde1.jpg', '40.0', 'Curso de língua alemã feito exclusivamente para aqueles que desejam adquirir o conhecimento do idioma. '),
(8, 'Espanhol', '/public-template/tema-site/img/courses/fundoverde2.jpg', '20.0', 'Espanhol básico. Este curso tem por objetivo apresentar a língua espanhola para aqueles que desejam cargos profissionais específicos.'),
(9, 'Francês', '/public-template/tema-site/img/courses/fundoverde3.jpg', '35.0', 'Curso de nível intermediário destinado às crianças.'),
(10, 'Japonês', '/public-template/tema-site/img/courses/fundoverde4.jpg', '42.0', 'Curso intermediário de japonês para pessoas que desejam aprender alfabetos e ideogramas variados.'),
(11, 'Italiano', '/public-template/tema-site/img/courses/fundoverde5.jpg', '35.0', 'Curso básico de italiano voltado para idosos.'),
(12, 'Inglês', '/public-template/tema-site/img/courses/fundoverde6.jpg', '25.4', 'Estruturas básicas e intermediárias da língua inglesa de maneira prática e objetiva.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `team`
--

CREATE TABLE `team` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `member_photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `team`
--

INSERT INTO `team` (`member_id`, `member_name`, `member_photo`, `member_description`) VALUES
(2, 'Flávia Macedo', '/public-template/tema-site/img/team/flavia.JPG', 'Flávia Macedo, aluno no Instituto Federal de São Paulo - IFSP.\r\nGU3003418');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `password_hash`, `user_full_name`, `user_email`) VALUES
(1, 'flavia', '$2y$10$s8XfjiSQ.oByDNMHyg1jGObNy8h8agEY1GP8jDzlZH/7mKvOpJnci', 'Flavia Macedo', 'flavia.lima@aluno.ifsp.edu.br'),
(2, 'flavia', '$2y$10$s8XfjiSQ.oByDNMHyg1jGObNy8h8agEY1GP8jDzlZH/7mKvOpJnci', 'Flavia Macedo', 'flavia.lima@aluno.ifsp.edu.br'),
(3, 'harrypotter', '$2y$10$XTu9a890Nm7TCf3GfoNRxOU.Vr7CCUpi9DLB/IVmZ7BHMYYCXORBu', 'Harry Potter', 'mbl.flavia@gmail.com');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Índices para tabela `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`member_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `team`
--
ALTER TABLE `team`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
