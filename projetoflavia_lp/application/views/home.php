<body>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

    <img src="public-template/tema-site/img/colecao/logospeaker1.png" alt="logo Speaker!" title="logo Speaker!" width="65" height="55" align="left"/>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>speaker" target="_self"> Speaker!</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>restrict" target="_self">
            <i class="fas fa-sign-in-alt"></i> Login
            </a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a href="https://www.linkedin.com/in/flaviamblima/" class="nav-link"
              target="_blank">
              <i class="fab fa-linkedin mr-2"></i>LinkedIn
            </a>
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

  <!--Carousel Wrapper-->
  <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

    <!--Indicators-->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-1z" data-slide-to="1"></li>
    </ol>
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

      <!--First slide-->
      <div class="carousel-item active">
        <div class="view" style="background-image: url('public-template/tema-site/img/colecao/img6.jpg'); background-repeat: no-repeat; background-size: cover;">

          <!-- Mask & flexbox options-->
          <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

            <!-- Content -->
            <div class="text-center white-text mx-5 wow fadeIn">
              <h1 class="mb-4">
                <strong>Aprenda um novo idioma com Speaker!</strong>
              </h1>

              <p>
                <strong>Um jeito prático e objetivo de aprender um novo idioma.</strong>
              </p>


              <a target="_blank" href="#Cursos" class="btn  aqua-gradient btn-lg">
                Demais!
                <i class="fas fa-star"></i>
              </a>
            </div>
            <!-- Content -->

          </div>
          <!-- Mask & flexbox options-->

        </div>
      </div>
      <!--/First slide-->

      <!--Second slide-->
      <div class="carousel-item">
        <div class="view" style="background-image: url('public-template/tema-site/img/colecao/img8.jpg'); background-repeat: no-repeat; background-size: cover;">

          <!-- Mask & flexbox options-->
          <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

            <!-- Content -->
            <div class="text-center white-text mx-5 wow fadeIn">
              <h1 class="mb-4">
                <strong>Aprenda um novo idioma com Speaker!</strong>
              </h1>

              <p>
                <strong>Um jeito prático e objetivo de aprender um novo idioma.</strong>
              </p>


              <a href="#Cursos" class="btn  aqua-gradient btn-lg">
                Demais!
                <i class="fas fa-star"></i>
              </a>
            </div>
            <!-- Content -->


          </div>
          <!-- Mask & flexbox options-->

        </div>
      </div>
      <!--/Second slide-->

    </div>
    <!--/.Slides-->

    <!--Controls-->
    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Próximo</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <!--/.Controls-->

  </div>
  <!--/.Carousel Wrapper-->

  <!--Main layout-->
  <main>
    <div class="container">

      <hr class="my-5">

      <section id="about" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>SOBRE O Speaker!</h2>
							<p>O Speaker! é uma plataforma de cursos que tem por objetivo facilitar o processo de aprendizagem e despertar o interesse das pessoas em falar outros idiomas.</p>
						</div>
					</div>
				</div>
				 
				</div>
			</div>
			<!-- /.container -->
		</section>

      <hr class="my-5">

      <!--SEÇÃO PORTFOLIO-->
      <section id="course" class="light-bg">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="section-title">
						<h2 class="Cursos" id="Cursos">Cursos</h2>
						<p>Aqui você encontra cursos rápidos e de excelente qualidade de idiomas variados!.</p>
            <br/>
					</div>
				</div>
			</div>
			<div class="row">

      <?php 
				if (!empty($courses)) {
					foreach ($courses as $course) { ?>
						<div class="col-sm-3">
							<div class="ot-course-item">
								<figure class="effect-bubba">
                  
                  
									<figcaption>
        
                      <!--Zoom effect-->
                      
                        <a class="text-center overlay zoom" href="#" data-toggle="modal" data-target="#course_<?=$course["course_id"]?>">
                          <img src="<?=base_url().$course["course_img"]?>" alt="img02" class="img-responsive center-block"/>
                        </a>
                      

                    
									</figcaption>
								</figure>
							</div>
            </div> 
            <!-- Até aqui funciona -->

            <!--MODAL após clicar no curso-->
            <!-- Modal -->
            <div class="modal fade" id="course_<?=$course["course_id"]?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
              aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">

                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?=$course["course_name"]?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                  <div class="modal-body">
                    <img src="<?=base_url().$course["course_img"]?>" alt="img01" class="img-responsive center-block" />
                    <div class="modal-works"><span>Duração: <?=intval($course["course_duration"])?> (horas)</span></div>
										<p><?=$course["course_description"]?></p>
                  </div>
                  
                  <div class="modal-footer">
                    <button type="button" class="btn aqua-gradient" data-dismiss="modal">Voltar</button>
                  </div>

                </div>
              </div>
            </div>

            <?php } //FOREACH
          }//IF ?>
                
			</div>
			</div><!-- end container -->
		</section> <!--fim port-->

      <hr class="mb-5">

      <section id="autora" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>Desenvolvido por</h2>
							<p>Equipe Speaker!.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- autora member item -->
					<div class="col-md-4">
						<div class="autora-item">
							<div class="autora-image">
								<img class="img-fluid z-depth-1 rounded-circle" src="public-template/tema-site/img/colecao/flavia.jpg" class="img-responsive" alt="Flávia Macedo" height="100px" width="100px" title="Flávia Macedo">
							</div>
              <br/>
							<div class="autora-text">
								<h3>Flávia Macedo</h3>
								<div class="autora-location">São Paulo, Brasil</div>
								<div class="autora-position">– Aluna no IFSP –</div>
                <p>Flávia Macedo B. Lima, Análise e Desenvolvimento de Sistemas, 3º semestre</p>
                <p>Instituto Federal de Educação, Ciência e Tecnologia de São Paulo</p>
                <p>Campus Guarulhos</p>
                <p>GU3003418</p>
							</div>
						</div>
					</div>
					<!-- end autora item -->
					 
						</div>
      </div>
      
		</section>
		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>Entre em contato!</h2>
							<p>Se você tem dúvidas ou sugestões, por favor, entre em contato.<br>Estarei aqui para responder!</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<h3>Local</h3>
						<p>Penha, São Paulo - SP</p>
						<p><i class="fa fa-envelope"></i> flavia.lima@aluno.ifsp.edu.br</p>
					</div>
					<div class="col-md-3">
						<h3>Horário de Atendimento</h3>
						<p><i class="far fa-clock"></i> <span class="day">Segunda à sexta:</span><span> 9h - 17h</span></p>
						<p><i class="far fa-clock"></i> <span class="day">Fins de semana:</span><span> Indisponível</span></p>
					</div>
					<div class="col-md-6">
						<form name="sentMessage" id="contactForm" novalidate="">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Nome" id="nome" required="" data-validation-required-message="Digite seu nome.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email" id="email" required="" data-validation-required-message="Digite seu email.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" placeholder="Mensagem" id="mensagem" required="" data-validation-required-message="Por favor, escreva a sua mensagem aqui."></textarea>
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center">
									<div id="success"></div>
                  <button type="submit" class="btn aqua-gradient">Enviar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<p id="back-top">
			<a href="#top"><button class="btn tempting-azure-gradient cyan-text" allign="right"><i class="fas fa-arrow-up"></i></button></a>
		</p>

    </div>
  </main>
  <!--Main layout-->
