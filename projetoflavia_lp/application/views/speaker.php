<!--Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark black">
  <img src="public-template/tema-site/img/colecao/logospeaker1.png" alt="logo Speaker!" title="logo Speaker!" width="65" height="55" align="left"/>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>restrict" target="_self">
                <i class="fas fa-sign-in-alt"></i> Login
            </a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a href="https://www.linkedin.com/in/flaviamblima/" class="nav-link"
              target="_blank">
              <i class="fab fa-linkedin mr-2"></i>LinkedIn
        </a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->


<section style="min-height: calc(100vh - 83px);" class="light-bg">

    <br/><br/><br/><br/>
  <center>
    <h3 class="titulo-speaker">Speaker!</h3>
      <span class="firsttext" id="firsttext">
           A Speaker! é uma empresa nova que traz consigo uma nova abordagem e novos métodos para desenvolver a habilidade de
           falar outros idiomas, seja por interesse pessoal ou por motivos profissionais. A Speaker! é uma alternativa acessível e
           objetiva para todos aqueles que se permitem experienciar coisas novas. Venha fazer parte!
      </span>
      <br/>
      <span class="secondtext" id="secondtext">
           Bons Estudos! - Equipe Speaker!
      </span>

      <br/><br/>
      <h4 class="titulo-speaker">Confira abaixo um vídeo sobre trava-línguas em outros idiomas!</h4>


      <br/>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/UflXcNC607E" frameborder="0" 
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <br/><br/>
  </center>



</section>