<!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public-template/tema-site/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public-template/tema-site/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public-template/tema-site/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public-template/tema-site/js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

<?php

      if(isset($scripts)){
        foreach ($scripts as $script_name){
          $src = base_url() . "public-template/tema-site/js/" . $script_name; ?>
            <script src="<?=$src?>"></script>
          <?php }
      } ?>
</body>

</html>