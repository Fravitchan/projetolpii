<!--Footer-->
<footer class="page-footer text-center font-small teal mt-4 wow fadeIn">

<hr class="my-4">

<!-- Social icons -->
<div class="pb-4">
  <a href="https://www.linkedin.com/in/flaviamblima/" target="_blank">
    <i class="fab fa-linkedin mr-3"></i>Flávia Macedo
  </a>
</div>
<!-- Social icons -->

<!--Copyright-->
<div class="footer-copyright py-3">
  © 2019 Copyright:
  <a href="<?php echo base_url(); ?>public-template/tema-site/https://mdbootstrap.com/education/bootstrap/" target="_blank"> MDBootstrap.com </a>
</div>
<!--/.Copyright-->

</footer>
<!--/.Footer-->


