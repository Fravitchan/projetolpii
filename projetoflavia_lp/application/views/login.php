<!--Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark black">
  <img src="public-template/tema-site/img/colecao/logospeaker1.png" alt="logo Speaker!" title="logo Speaker!" width="65" height="55" align="left"/>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>speaker" target="_self"> Speaker!</a>
      </li>
       
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a href="https://www.linkedin.com/in/flaviamblima/" class="nav-link"
              target="_blank">
              <i class="fab fa-linkedin mr-2"></i>LinkedIn
        </a>
      </li>
       
       
    </ul>
  </div>
</nav>
<!--/.Navbar -->

<!--LOGIN-->
<section style="min-height: calc(100vh - 83px);" class="light-bg">
    <div class="container">
      <div class="row">
        <div class="col-6 col-md-6 text-center">
            <div class="section-title">
                <br/><br/><br/><br/>
                <h2>LOGIN</h2>

                <form id="login_form" method="post">

                <!-- USUÁRIO -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                      <a class="btn-floating btn-lg tempting-azure-gradient white-text"><i class="fas fa-user-ninja"></i></a>
                                    </div>
                                    <input type="text" placeholder="Usuário" id="username" name="username" class="form-control">
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                <!-- FIM USUÁRIO -->

                <!-- SENHA --->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                          <a class="btn-floating btn-lg tempting-azure-gradient white-text"><i class="fas fa-key"></i></a>
                                        </div>
                                        <input type="password" placeholder="Senha" name="password" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                <!--FIM SENHA-->

                <!--ENVIAR-->    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button type="submit" id="btn_login" class="btn aqua-gradient">Login</button>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                <!-- FIM ENVIAR-->

                </form>
            </div>
        </div>
    </div>
</div>
</section>
<!--/.LOGIN-->